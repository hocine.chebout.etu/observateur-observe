package fr.univlille.iut.r304.tp4.q1;

public class Main {
    public static void main(String[] args){
        Chrono c1 = new Chrono();
        Chrono c2 = new Chrono("MonChrono",5);
        Timer t1 = new Timer();
        t1.attach(c1);
        t1.attach(c2);
        t1.start();
    

    }
    
}
