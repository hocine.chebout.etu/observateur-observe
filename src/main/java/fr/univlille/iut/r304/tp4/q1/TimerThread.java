package fr.univlille.iut.r304.tp4.q1;

public class TimerThread extends Thread {
    private Timer timer;



    public void setTimer(Timer timer) {
        this.timer = timer;
    }
    int cpt =0;
    @Override
    public void run(){
        while (true) {
            
            try {
                sleep(1000);
                this.timer.notifyObservers();
                
                cpt++;
                
                // annoncer le « tick-horloge »
            } catch (InterruptedException e) {
                // on ignore et on espère que ce n’est pas grave
            }
        }
    }
}
