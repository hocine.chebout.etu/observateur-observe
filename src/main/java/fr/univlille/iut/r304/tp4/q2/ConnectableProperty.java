package fr.univlille.iut.r304.tp4.q2;

import fr.univlille.iut.r304.tp4.q1.Observer;
import fr.univlille.iut.r304.tp4.q1.Subject;

public class ConnectableProperty extends ObservableProperty implements Observer {

	ConnectableProperty otherConnectableProperty;

	public void connectTo(ConnectableProperty other) {
		other.attach(this);
	}

	public void biconnectTo(ConnectableProperty other) {
		other.attach(this);
		this.attach(other);
	}

	public void unconnectFrom(ConnectableProperty other) {
		this.detach(other);
	}

	public void update(Subject subj) {
		this.update(subj);
    }

    public void update(Subject subj, Object data) {
        this.update(subj,data);
    }

}