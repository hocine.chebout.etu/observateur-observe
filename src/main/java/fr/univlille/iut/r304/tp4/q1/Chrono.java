package fr.univlille.iut.r304.tp4.q1;
public class Chrono implements Observer {
    private int timeTotal;
    private String name;
    private static int CTP;

    public int getTimeTotal() {
        return this.timeTotal;
    }

    public void setTimeTotal(int timeTotal) {
        this.timeTotal = timeTotal;
    }

    public Chrono(String name,int initTime) {
        this.name = name +""+ CTP++ +" : ";
        this.timeTotal = initTime;
    }

    public Chrono() {
        this("Chrono",0);
    }

    public void update(Subject subj) {
        this.timeTotal++;
        System.out.println(this.toString());
    }

    public void update(Subject subj, Object data) {
        this.update(subj);
    }

    public String afficheTime(){
        return this.name +" "+ this.timeTotal;
        
    }

    public String toString() {
        return this.name + this.timeTotal;
    }
    
}