package fr.univlille.iut.r304.tp4.q1;

public class Timer extends Subject {
	private TimerThread thread; 

	public void start() {
		this.thread = new TimerThread();
		this.thread.setTimer(this);
		this.thread.start();
	}

	public void stopRunning() {
		thread.interrupt();
	}
}