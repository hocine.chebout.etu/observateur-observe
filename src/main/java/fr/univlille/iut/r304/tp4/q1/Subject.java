package fr.univlille.iut.r304.tp4.q1;

import java.util.HashSet;
import java.util.Set;

public abstract class Subject {

	Set<Observer> observers = new HashSet<Observer>();

	protected void notifyObservers() {
		for (Observer obs : this.observers) {
			obs.update(this);
		}
	}

	protected void notifyObservers(Object data) {
		for (Observer observer : this.observers) {
			observer.update(this, data);
		}
	}

	public void attach(Observer observer) {
		this.observers.add(observer);
	}

	public void detach(Observer observer) {
		this.observers.remove(observer);
	}
}