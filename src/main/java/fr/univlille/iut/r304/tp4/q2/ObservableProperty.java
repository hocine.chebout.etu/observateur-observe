package fr.univlille.iut.r304.tp4.q2;

import fr.univlille.iut.r304.tp4.q1.Subject;

public class ObservableProperty extends Subject{

	private Object value;

	public void setValue(Object i) {
		this.value = i;
		this.notifyObservers(i);
	}

	public Object getValue() {
		return this.value;
	}

}